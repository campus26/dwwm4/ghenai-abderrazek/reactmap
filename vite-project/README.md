# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## handleSubmit

La fonction handleSubmit est une fonction de gestionnaire d'événements dans le composant AddressForm. Elle est appelée lorsque le formulaire est soumis. Son rôle principal est de gérer la soumission du formulaire, en envoyant une requête à l'API d'OpenRouteService pour rechercher l'adresse en fonction des informations saisies dans le formulaire.
