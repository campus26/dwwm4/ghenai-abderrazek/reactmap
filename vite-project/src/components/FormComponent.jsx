import { useState, useEffect } from 'react';
import { MapComponent } from './MapComponent'; // Importe le composant MapComponent

export function FormComponent(routeCoordinates, polylineCoords) {
  const [start, setStart] = useState(''); // Définit un état local pour stocker le nom de la ville
  const [end, setEnd] = useState(''); // Définit un état local pour stocker le nom de la ville
  const [startAddress, setStartAddress] = useState(null); // Définit un état local pour stocker les coordonnées de départ
  const [endAddress, setEndAddress] = useState(null); // Définit un état local pour stocker les coordonnées d'arrivée


  // Fonction de gestion de la soumission du formulaire
  const handleSubmit = async (e) => {
    e.preventDefault(); // Empêche le comportement par défaut du formulaire (rechargement de la page)
    try {
      let addressFound = false
      // Envoie d'une requête à l'API OpenRouteService pour rechercher l'adresse en fonction du nom de la ville
           if (start.trim() !== '') {
      const startResponse = await fetch(`https://api.openrouteservice.org/geocode/search?api_key=5b3ce3597851110001cf62486cad5f112fcf4a53826dc196a69adf75&text=${start}`);
      const startData = await startResponse.json();  // Convertit la réponse en JSON
      console.log(startData);
      if (startData.features && startData.features.length > 0) {
        const startFirstResult = startData.features[0];
        setStartAddress(startFirstResult.geometry.coordinates);
        addressFound = true;
      }
      else {
        alert("Adresse non trouvée")
      }
    }
  
  
      // Vérifie si le champ de l'input pour l'arrivée n'est pas vide
      if (end.trim() !== '') {
        const endResponse = await fetch(`https://api.openrouteservice.org/geocode/search?api_key=5b3ce3597851110001cf62486cad5f112fcf4a53826dc196a69adf75&text=${end}`);
        const endData = await endResponse.json(); // Convertit la réponse en JSON
        if (endData.features && endData.features.length > 0) {
          const endFirstResult = endData.features[0];
          setEndAddress(endFirstResult.geometry.coordinates);
        }
        // Vérifie si les adresses sont disponibles
        else {
          alert("Adresse B non trouvée")
        }
      
     
      }
     
   
    } catch (error) {
      console.error('Erreur lors de la recherche de l\'adresse :', error); // Affiche les erreurs dans la console s'il y en a
    }
    
    
  };
  
  return (
    
    <>
    <MapComponent startAddress={startAddress} endAddress={endAddress} routeCoordinates={routeCoordinates} polylineCoords={polylineCoords} />
    <div className="flex-1">
  <h2 className="bg-red-700 px-4 py-2 text-white font-bold">EDN Cartographie</h2>
  <form className="px-4 py-6" onSubmit={handleSubmit}>
    <label className="flex items-center text-gray-600 text-lg mb-4">
      <span className="mr-2">&#8942;</span> A
      <input className="border border-gray-300 rounded px-3 py-1 ml-2 w-full" type="text" value={start} onChange={(e) => setStart(e.target.value)} />
    </label>
    <label className="flex items-center text-gray-600 text-lg mb-4">
      <span className="mr-2">&#8942;</span> B
      <input className="border border-gray-300 rounded px-3 py-1 ml-2 w-full" type="text" value={end} onChange={(e) => setEnd(e.target.value)} />
    </label>
    <button className="bg-gray-300 hover:bg-gray-400 text-gray-600 py-1 px-4 rounded float-right" type="submit">Rechercher</button>
   
  </form>
  
</div>
    
    </>
    
  );
}
