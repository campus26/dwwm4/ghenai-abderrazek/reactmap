import { useEffect, useState, useRef } from 'react'; // Importe les hooks nécessaires depuis React
import { MapContainer, TileLayer, Marker, Popup, ZoomControl, Polyline } from 'react-leaflet'; // Importe les composants nécessaires depuis react-leaflet

export function MapComponent({ startAddress, endAddress}) { // Définit un composant fonctionnel MapComponent qui reçoit une prop 'address'
  
  const [startPosition, setStartPosition] = useState([45.0425, 3.8858]); // Définit un état local pour stocker la position du marqueur
  
  const [endPosition, setEndPosition] = useState(null);
  const [routeCoordinates, setRouteCoordinates] = useState(null);
  const [polylineCoords, setPolylineCoords] = useState(null);
  const mapRef = useRef(null); // Crée une référence mutable pour le composant MapContainer

  useEffect(() => { 
    if (startAddress) {
      setStartPosition([startAddress[1], startAddress[0]]);
    }
  
    if (endAddress) {
      setEndPosition([endAddress[1], endAddress[0]]);
    }
    if (mapRef.current && startPosition) {
      mapRef.current.flyTo(startPosition, 13); // Centre la carte sur la position du premier marqueur avec un zoom fixe de 13
    }
    const fetchPolylineCoords = async () => {
      try {
    if (startAddress && endAddress) {
      const routeResponse = await fetch(`https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf62486cad5f112fcf4a53826dc196a69adf75&start=${startAddress}&end=${endAddress}`);
      const routeData = await routeResponse.json();
      if (routeData.features && routeData.features.length > 0) {
        const route = routeData.features[0].geometry.coordinates;
        setRouteCoordinates(route);
        const polylineCoords = route.map(coord => [coord[1], coord[0]]);
        setPolylineCoords(polylineCoords);
      }
    }
  } catch (error) {
    console.error('Erreur lors de la récupération de la polyline :', error);
  }
};

fetchPolylineCoords();
  
  }, [startAddress, endAddress]);



  
  return (
    <>
    
    <MapContainer ref={mapRef} center={startPosition} zoom={13} zoomControl={false}  className="flex-3"> 
      <TileLayer // Ajoute une couche de tuiles à la carte
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" 
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' 
      />
         
       {startPosition && (
        <Marker position={startPosition}>
          <Popup>Départ</Popup>
        </Marker>
      )}
      {endPosition && (
        <Marker position={endPosition}>
          <Popup>Arrivée</Popup>
        </Marker>
      )}
   
         {polylineCoords && (
        <Polyline positions={polylineCoords} color="red" />
      )}
      
     
      <ZoomControl position="topright" />
    </MapContainer>
  
    </>
  );
}
